package com.work.virtuallibrary;

import com.work.virtuallibrary.entity.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LibRestIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void findBookById(){

        //act

        var book=restTemplate.getForObject("http://localhost:" + port + "library/books/775", Book.class);

        //assert

        assertThat(book)
                .extracting(Book::getId,Book::getTitle)
                 .containsExactly(775L,"5 weeks in baloon");
    }



}
