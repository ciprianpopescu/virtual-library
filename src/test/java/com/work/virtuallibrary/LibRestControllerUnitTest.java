package com.work.virtuallibrary;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.work.virtuallibrary.entity.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class LibRestControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void testGetAllBooks() throws Exception {
       mockMvc.perform(MockMvcRequestBuilders.get("/library/books")
               .accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk());
    }

    @Test
    public void testGetAllUsers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/library/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


    @Test
    public void testGetAllAllAuthors() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/library/authors")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetBookWithTitle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/library/books/book")
                .param("title","5 weeks in baloon")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


    @Test
    public void testGetBookWithBadTitle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/library/books/book")
                .param("title","Some Random Title")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }


//    @Test
//    public void testGetSingleBook() throws Exception {
//
//        mockMvc.perform(MockMvcRequestBuilders.get("/library/books/{bookId}")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(book)))
//                .andExpect(status().isOk());
//    }

    @Test
    public void testPostBook() throws Exception {
        Book book=new Book();
        book.setTitle("some Title");

        mockMvc.perform(MockMvcRequestBuilders.post("/library/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isNotFound());
    }

}
