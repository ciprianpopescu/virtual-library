package com.work.virtuallibrary.util;

import com.work.virtuallibrary.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduledTasks {

    @Autowired
    UserService userService;

    @Scheduled(cron = "0 0 1 * * ?")
    //once a day, at 1 o'clock
    public void markUserWithOverdueBooks() {
        userService.markUsersWithOverdueBooks();
    }


}
