package com.work.virtuallibrary.entity;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AccountCredentials {
    private String username;
    private String password;
}
