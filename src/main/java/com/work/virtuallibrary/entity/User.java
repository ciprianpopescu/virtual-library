package com.work.virtuallibrary.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Validated
@Table(uniqueConstraints=
@UniqueConstraint(columnNames = {"username"}))
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false)
    private String address;

    @Column(columnDefinition = "boolean default false", nullable = false)
    private boolean isLibrarian;

    @Column(columnDefinition = "boolean default false", nullable = false)
    private boolean hasOverdueBooks;


    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String role;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "user")
    @JsonBackReference
    private List<Book> books;
}
