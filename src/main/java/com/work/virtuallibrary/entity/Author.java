package com.work.virtuallibrary.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;

@Entity
@Table(uniqueConstraints=
@UniqueConstraint(columnNames = {"name", "surname"}))
@NoArgsConstructor
@Getter @Setter
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "author_id")
    private Long id;


    @NotEmpty(message = "Please provide a name")
    @Column(nullable = false)
    private String name;

    @NotEmpty(message = "Please provide a surname")
    @Column(nullable = false)
    private String surname;

    @Override
    public String toString() {
        return  name +" "+  surname  ;
    }

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "author")
    @JsonBackReference
    private Collection<Book> books;
}

