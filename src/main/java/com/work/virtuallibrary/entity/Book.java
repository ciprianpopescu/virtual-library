package com.work.virtuallibrary.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Entity
@NoArgsConstructor
@Table(name = "book")
@AllArgsConstructor
@Data
@Validated
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "book_id")
    private Long id;

    @Column(nullable = false)
    @NotEmpty(message = "Please provide a title")
    private String title;

    @Column(nullable = true)
    private String description;

    @Column(columnDefinition = "boolean default false", nullable = false)
    private Boolean isBorrowed = false;

    @Column(nullable = true)
    private Date startDateOfBorrowing;

    @Column(nullable = true)
    private Date endDateOfBorrowing;

    @Column(columnDefinition = "int default 14", nullable = false)
    private int BorrowingDays;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id",nullable = false)
   // @JsonManagedReference
    //removed due to com.fasterxml.jackson.databind.exc.InvalidDefinitionException: Cannot handle managed/back reference 'defaultReference':
    // back reference type (java.util.Collection) not compatible with managed type (com.work.virtuallibrary.entity.Book)
    private Author author;

    @ManyToOne(fetch = FetchType.EAGER,optional = true)
    @JoinColumn(name = "user_id")
    private User user;

}
