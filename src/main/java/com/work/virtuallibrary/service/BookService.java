package com.work.virtuallibrary.service;

import com.work.virtuallibrary.entity.Author;
import com.work.virtuallibrary.entity.Book;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface BookService {
    Book save(Book book);

    Iterable<Book> findAll();

    Iterable<Book> findAll(Sort sort);

    void deleteById(long id);

    Book findById(Long id);

    List<Book> findByTitleContaining(String title);

    Book findByTitle(String title);

    Optional<Book> findBookByAuthorAndTitle(Author author, String title);




}
