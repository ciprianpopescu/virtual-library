package com.work.virtuallibrary.service;

import com.work.virtuallibrary.entity.Author;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface AuthorService {
    Author save(Author book);

    Iterable<Author> findAll();


    Iterable<Author> findAll(Sort sort);

    void deleteById(long id);

    Author findById(Long id);

    Author findAuthorByNameAndSurname(String name, String surname);

     List<Author>findAuthorBySurnameContaining(String name);

    //   Page<Author> findAuthorBySurnameContaining(String surname, Pageable pageable);<- pagination example

}
