package com.work.virtuallibrary.service;

import com.work.virtuallibrary.entity.Author;
import com.work.virtuallibrary.entity.Book;
import com.work.virtuallibrary.exception.BookAlreadyExistsException;
import com.work.virtuallibrary.exception.RecordNotFoundException;
import com.work.virtuallibrary.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
@Validated
public class BookServiceImpl implements BookService {


    @Autowired
    private BookRepository bookRepository;

    @Override
    public Book save(Book book) {
        try {
            if (book.getAuthor() == null) {
                throw new RecordNotFoundException("Author cannot be null");
            } else {
                Optional<Book> dbBook = findBookByAuthorAndTitle(book.getAuthor(), book.getTitle());
                if (dbBook.isPresent()) {
                    throw new BookAlreadyExistsException("Book with same author and title already exists");
                } else
                    return bookRepository.save(book);
            }
        }catch (DataIntegrityViolationException e) {
            throw new DataIntegrityViolationException("Book already exists");
        }
    }

    @Override
    public Iterable<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public Iterable<Book> findAll(Sort sort) {
        if (StreamSupport.stream(bookRepository.findAll().spliterator(), false).count() != 0) {
            return bookRepository.findAll(sort);
        } else {
            throw new RecordNotFoundException("No book is present in database");
        }
    }

    @Override
    public void deleteById(long id) {
        try {
            bookRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new RecordNotFoundException("Book id '" + id + "' does no exist");
        }
    }

    @Override
    public Book findById(Long id) {
        return bookRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Book id '" + id + "' does no exist"));
    }


    public List<Book> findByTitleContaining(String title) {
        return bookRepository.findBookByTitleContaining(title)
                .orElseThrow(() -> new RecordNotFoundException("Book title '" + title + "' does no exist"));
    }

    @Override
    public Book findByTitle(String title) {
        return bookRepository.findByTitle(title)
                .orElseThrow(() -> new RecordNotFoundException("Book with title '" + title + "' does no exist"));
    }

    @Override
    public Optional<Book> findBookByAuthorAndTitle(Author author, String title) {
        return bookRepository.findBookByAuthorAndTitle(author, title);

    }
}


