package com.work.virtuallibrary.service;

import com.work.virtuallibrary.entity.Author;
import com.work.virtuallibrary.exception.RecordNotFoundException;
import com.work.virtuallibrary.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.StreamSupport;

@Service
@Validated
public class AuthorServiceImpl implements AuthorService {


    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public Author save(Author author) {
        try {
            return authorRepository.save(author);
        } catch (DataIntegrityViolationException e) {
            throw new DataIntegrityViolationException("Author already exists");
        }
    }

    @Override
    public Iterable<Author> findAll() {
        if (StreamSupport.stream(authorRepository.findAll().spliterator(), false).count() != 0) {
            return authorRepository.findAll();
        } else {
            throw new RecordNotFoundException("No author is present in database");
        }
    }

    @Override
    public Iterable<Author> findAll(Sort sort) {

        if (StreamSupport.stream(authorRepository.findAll().spliterator(), false).count() != 0) {
            return authorRepository.findAll(sort);

        } else {
            throw new RecordNotFoundException("No author is present in database");
        }
    }


    @Override
    public void deleteById(long id) {
        try {
            authorRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new RecordNotFoundException("Author id '" + id + "' does no exist");
        } catch (DataIntegrityViolationException e) {
            throw new DataIntegrityViolationException("Author id '" + id + "' has books in database");
        }


    }

    @Override
    public Author findById(Long id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Author id '" + id + "' does no exist"));
    }

    @Override
    public Author findAuthorByNameAndSurname(String name, String surname) {
        return authorRepository.findAuthorByNameAndSurname(name, surname)
                .orElseThrow(() -> new RecordNotFoundException("Author with name '" + name +
                        "' and surname '" + surname + "' does no exist"));
    }

    @Override
    public List<Author> findAuthorBySurnameContaining(String surname) {
        return authorRepository.findAuthorBySurnameContaining(surname)
                .orElseThrow(() -> new RecordNotFoundException("Author with surname containing'" + surname + "' does no exist"));
    }


}


