package com.work.virtuallibrary.service;

import com.work.virtuallibrary.entity.User;


public interface UserService {
    User save(User user);

    Iterable<User> findAll();

    void deleteById(long id);

    User findById(Long id);

    void borrowABook(Long userId, String title);

    void unBorrowABook(Long userId, String title);

    void markUsersWithOverdueBooks();

   // List<User> getUsersWithOverdueBooks();

}
