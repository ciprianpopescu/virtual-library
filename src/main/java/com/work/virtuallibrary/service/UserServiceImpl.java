package com.work.virtuallibrary.service;

import com.work.virtuallibrary.entity.Book;
import com.work.virtuallibrary.entity.User;
import com.work.virtuallibrary.exception.BookBorrowedException;
import com.work.virtuallibrary.exception.RecordNotFoundException;
import com.work.virtuallibrary.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BookService bookService;

    @Override
    public User save(User user) {
        try {
            return userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new DataIntegrityViolationException("User already exists");
        }
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void deleteById(long id) {
        long count = StreamSupport.stream(bookService.findAll().spliterator(), false)
                .filter(f -> f.getUser() != null)
                .filter(f -> f.getUser().getId() == id)
                .count();
        if (count > 0) {
            throw new DataIntegrityViolationException("User with id '" + id + "' has borrowed books ");
        } else {
            try {
                userRepository.deleteById(id);
            } catch (EmptyResultDataAccessException e) {
                throw new RecordNotFoundException("User id '" + id + "' does no exist");
            }
        }

    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("User id '" + id + "' does no exist"));
    }


    @Override
    public void borrowABook(Long userId, String bookTitle) {
        User user = findById(userId);
        Book bookToBorrow = bookService.findByTitle(bookTitle);
        if (!bookToBorrow.getIsBorrowed()) {
            user.getBooks().add(bookToBorrow);
            save(user);
            updateBorrowedBook(bookToBorrow, user);
        } else
            throw new BookBorrowedException("Book is already borrowed");
    }

    @Override
    public void unBorrowABook(Long userId, String bookTitle) {
        User user = findById(userId);
        Book bookToUnborrow = bookService.findByTitle(bookTitle);
        if (bookToUnborrow.getIsBorrowed()) {
            user.getBooks().remove(bookToUnborrow);
            save(user);
            updateUnborrowedBook(bookToUnborrow);
        } else
            throw new BookBorrowedException("Book is not borrowed");

    }

    @Override
    public void markUsersWithOverdueBooks() {
        Optional.of(StreamSupport.stream(bookService.findAll().spliterator(), false)
                .filter(b -> b.getEndDateOfBorrowing() != null)
                .filter(b -> b.getEndDateOfBorrowing().compareTo(new Date()) > 0)
                .collect(Collectors.toList()))
                .ifPresent(books -> books
                        .forEach(b -> {
                            User user = b.getUser();
                            user.setHasOverdueBooks(true);
                            userRepository.save(user);
                        }));
    }

    private void updateBorrowedBook(Book book, User user) {
        book.setIsBorrowed(true);
        book.setStartDateOfBorrowing(Date.from(Instant.now()));
        Date endDate = Date.from(book.getStartDateOfBorrowing().toInstant()
                .plus(book.getBorrowingDays(), ChronoUnit.DAYS));
        book.setEndDateOfBorrowing(endDate);
        book.setUser(user);
        bookService.save(book);
    }


    private void updateUnborrowedBook(Book book) {
        book.setIsBorrowed(false);
        book.setStartDateOfBorrowing(null);
        book.setEndDateOfBorrowing(null);
        book.setUser(null);
        bookService.save(book);
    }
}

