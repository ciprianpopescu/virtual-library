package com.work.virtuallibrary.repository;

import com.work.virtuallibrary.entity.Author;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface AuthorRepository extends PagingAndSortingRepository<Author, Long>
//it extends Crud repo with paging and sorting
{
    Optional<Author> findAuthorByNameAndSurname(String name, String surname);

    Optional<List<Author>> findAuthorBySurnameContaining(String surname);

    Iterable<Author> findAll();

    @Override
    Iterable<Author> findAll(Sort sort);
}
