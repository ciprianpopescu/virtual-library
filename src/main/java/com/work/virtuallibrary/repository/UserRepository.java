package com.work.virtuallibrary.repository;

import com.work.virtuallibrary.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {

    User findByUsername(String username);

    }
