package com.work.virtuallibrary.repository;

import com.work.virtuallibrary.entity.Author;
import com.work.virtuallibrary.entity.Book;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;


public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
    //    @Query("SELECT b FROM Book b WHERE b.title LIKE %:title%")
    Optional<List<Book>> findBookByTitleContaining(String title);

    @Override
    Iterable<Book> findAll(Sort sort);

    Optional<Book> findByTitle(String title);
    Optional<Book> findBookByAuthorAndTitle(Author author, String title);




}
