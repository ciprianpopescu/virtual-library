package com.work.virtuallibrary.exception;

public class BookBorrowedException extends RuntimeException {
    public BookBorrowedException(String message) {
        super(message);
    }
}
