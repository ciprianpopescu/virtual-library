package com.work.virtuallibrary.web;

import com.work.virtuallibrary.entity.Author;
import com.work.virtuallibrary.entity.Book;
import com.work.virtuallibrary.entity.User;
import com.work.virtuallibrary.service.AuthorService;
import com.work.virtuallibrary.service.BookService;
import com.work.virtuallibrary.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
@RequestMapping("library")
public class LibRestController {
    @Autowired
    AuthorService authorService;

    @Autowired
    BookService bookService;

    @Autowired
    UserService userService;


    //Authors
    @PostMapping("/authors")
    public ResponseEntity<Author> addAuthor(@RequestBody Author author) {
        return new ResponseEntity<>(authorService.save(author), HttpStatus.OK);
    }


    @GetMapping("/authors")
    public ResponseEntity<Iterable<Author>> findAllAuthors() {
        return new ResponseEntity<>(authorService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/authors/sort")
    public ResponseEntity<Iterable<Author>> findAllAuthors(@RequestParam(value = "sortBy") String sortBy) {
        Sort sort = Sort.by(sortBy);
        return new ResponseEntity<>(authorService.findAll(sort), HttpStatus.OK);
    }

    @GetMapping("/authors/{authorId}")
    public ResponseEntity<Author> findAuthorById(@PathVariable @NumberFormat
                                                 @Min(value = 0,
                                                         message = "id must be greater than or equal to 0")
                                                         Long authorId) {
        return new ResponseEntity<>(authorService.findById(authorId), HttpStatus.OK);
    }


    @GetMapping("/authors/author")
    public ResponseEntity<Author> findAuthorByNameAndSurname(@RequestParam String name, @RequestParam String surname) {
        return new ResponseEntity<>(authorService.findAuthorByNameAndSurname(name, surname), HttpStatus.OK);

    }

    @GetMapping("/authors/containing")
    public ResponseEntity<Author> findAuthorBySurnameLike(@RequestParam String surname) {
        return new ResponseEntity(authorService
                .findAuthorBySurnameContaining(surname), HttpStatus.OK);
    }

    @DeleteMapping("/authors/{authorId}")
    public ResponseEntity<Author> deleteAuthor(@PathVariable @Min(value = 0,
            message = "id must be greater than or equal to 0")
                                                     Long authorId) {
        authorService.deleteById(authorId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    // Books

    @PostMapping("/books")
    public ResponseEntity<Book> addBook(@RequestBody Book book) {
        return new ResponseEntity<>(bookService.save(book), HttpStatus.OK);
    }

    @GetMapping("/books")
    public ResponseEntity<Iterable<Book>> findAllBooks() {
        return new ResponseEntity<>(bookService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/books/sort")
    public ResponseEntity<Iterable<Book>> findAllBooks(@RequestParam(value = "sortBy") String sortBy) {
        return new ResponseEntity<>(bookService.findAll(Sort.by(sortBy)), HttpStatus.OK);
    }

    @GetMapping("/books/{bookId}")
    public ResponseEntity<Book> findBookById(@PathVariable @Min(0) Long bookId) {
        return new ResponseEntity<>(bookService.findById(bookId), HttpStatus.OK);
    }

    @GetMapping("/books/book")
    @ResponseBody
    public ResponseEntity<List<Book>> findBookByTitle(@RequestParam String title) {
        return new ResponseEntity<>(bookService.findByTitleContaining(title), HttpStatus.OK);
    }

    @DeleteMapping("/books/{bookId}")
    public ResponseEntity<Book> deleteBook( @PathVariable @Min(value = 0,
            message = "id must be greater than or equal to 0")
                                                   Long bookId) {
        bookService.deleteById(bookId);
        return new ResponseEntity(HttpStatus.OK);
    }


    //Users
    @PostMapping("/users")
    public ResponseEntity addUser(@RequestBody User user) {
        User savedUser = userService.save(user);
        return new ResponseEntity(savedUser, HttpStatus.OK);
    }

    //
    @GetMapping("/users")
    public ResponseEntity<User> findAllUsers() {
        Iterable<User> allUsers = userService.findAll();
        return new ResponseEntity(allUsers, HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> findUserById(@PathVariable @NumberFormat @Min(value = 0,
            message = "id must be greater than or equal to 0") Long id) {
        return new ResponseEntity(userService.findById(id), HttpStatus.OK);
    }

    //
    @PostMapping("/users/{userId}/borrow")
    public ResponseEntity<User> borrowABook(@PathVariable @Min(0) Long userId, @RequestParam String title) {
        //TODO: title is not unique, modify
        userService.borrowABook(userId, title);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/users/{userId}/unborrow")
    public ResponseEntity<User> unborrowABook(@PathVariable @Min(0) Long userId, @RequestParam String title) {
        userService.unBorrowABook(userId, title);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<User> deleteUser(@PathVariable @Min(value = 0,
            message = "id must be greater than or equal to 0")
                                                   Long userId) {
        userService.deleteById(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
